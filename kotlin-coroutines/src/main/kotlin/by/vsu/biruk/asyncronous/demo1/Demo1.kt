package by.vsu.biruk.asyncronous.demo1

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeUnit

fun main() = runBlocking {

    val productNames = getProductNames()
    val productPrices = getProductPrices(productNames)

    val order = productNames.indices
        .map { productNames[it] to productPrices[it] }
        .filterNot { (name, price) -> isTVProduct(name) && isCheapProduct(price) }

    order.forEach { println(it) }
}

suspend fun getProductNames(): List<String> {

    delay(TimeUnit.SECONDS.toMillis(3))

    return listOf("Big plasma TV", "Old suitcase", "TV", "BMW")
}

suspend fun getProductPrices(productNames: List<String>): List<Int> {

    delay(TimeUnit.SECONDS.toMillis(3))

    return productNames.map { it.length }
}

fun isTVProduct(productName: String): Boolean = productName.contains("TV")

fun isCheapProduct(productPrice: Int): Boolean = productPrice < 5
