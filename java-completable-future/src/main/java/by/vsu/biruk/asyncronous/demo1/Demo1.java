package by.vsu.biruk.asyncronous.demo1;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Demo1 {

    public static void main(String[] args) {

        Map<String, Integer> order = getProductNames()
                .thenCompose(productNames -> getProductPrices(productNames)
                        .thenApply(productPrices -> IntStream.range(0, productNames.size()).boxed()
                                .collect(Collectors.toMap(productNames::get, productPrices::get))
                        )
                )
                .thenApply(productNameWithPrice -> productNameWithPrice.entrySet().stream())
                .thenApply(productNamePriceStream -> productNamePriceStream
                        .filter(productNameWithPrice -> !(isTVProduct(productNameWithPrice.getKey()) && isCheapProduct(productNameWithPrice.getValue())))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
                )
                .join();

        order.entrySet().forEach(System.out::println);
    }

    private static CompletableFuture<List<String>> getProductNames() {

        sleep(TimeUnit.SECONDS.toMillis(3));

        return CompletableFuture.completedFuture(Arrays.asList("Big plasma TV", "Old suitcase", "TV", "BMW"));
    }

    private static CompletableFuture<List<Integer>> getProductPrices(List<String> productNames) {

        sleep(TimeUnit.SECONDS.toMillis(3));

        return CompletableFuture.completedFuture(productNames.stream())
                .thenApply(productStream -> productStream.map(String::length))
                .thenApply(priceStream -> priceStream.collect(Collectors.toList()));
    }

    private static boolean isTVProduct(String productName) {

        return productName.contains("TV");
    }

    private static boolean isCheapProduct(Integer productPrice) {

        return productPrice < 5;
    }

    private static void sleep(Long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }
}
